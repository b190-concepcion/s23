
/*
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)

*/
//5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!


let Trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends:  {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I Choose You!");
	},
}
console.log(Trainer);


//6. Access the trainer object properties using dot and square bracket notation.
console.log("Result from dot notation: ");
console.log(Trainer.name);

console.log("Result from square brackets notation: ");
console.log(Trainer.pokemon);


console.log("Result from talk method: ")

//7. Invoke/call the trainer talk object method.
Trainer.talk();




/*
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
*/
function Pokemon(name, level){
	//Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;


	//Methods
	this.tackle = function(target){

		console.log(this.name + " tackled " + target.name);
		target.health -= this.attack;
		console.log(target.name + " is now reduced to " + target.health);
		
		if(target.health <= 0){
			target.health = 0;
			this.faint(target);
		}
		
	}
	this.faint = function(target){
		console.log(target.name + " fainted");
	}
}

/* 9. Create/instantiate several pokemon object from the constructor with varying name and level properties.*/
let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

/*
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.
*/

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodude);

// geodude.faint();

console.log(geodude);
