console.log("hello world");
//SECTION - Objects
/*
	-objects are data types that are used to represent real world objects
	- collection of related data and/or functionalitites
		Object literals = {}

		Syntax (using initializers):
		let/const objectName = {
		keyA: ValueA,
		keyB: valueB
		}
		*/


		let cellphone = {

			name: "Nokia 3210",
			manufacturedDate: 1999
		}

		console.log("Result from creating objects using initializers");
		console.log(cellphone);
		console.log(typeof cellphone);


		/*let car = {

			name: "Honda",
			model: "Brio",
			releasedDate: 2021
		}

		console.log("Result from creating objects using initializers");
		console.log(car);*/


//creating objects using constructor function
/*
	-creates a reusable function to create/construct several objects that have the same data structure

	Syntax:
	function ObjectName(keyA,keyB){
	this.keyA = keyA,
	this.keyB = keyB;
	}
	*/
/*
	-"this" keyword allows to assign a new property for the object by associating them with values received from a constructor functions parameters
	*/



	function Laptop(name,manufacturedDate){
		this.name = name,
		this.manufacturedDate = manufacturedDate;
	}
//"new" keyword creates an instance of an object
let laptop = new Laptop("Dell",2012)
console.log("Result from creating objects using initializers");
console.log(laptop);
console.log(typeof laptop);

//creates a new laptop object;
let laptop2 = new Laptop("Lenovo",2008)
console.log("Result from creating objects using initializers");
console.log(laptop2);
console.log(typeof laptop2);

//create empty objects
let computer = {};
let myComputer = new Object();

console.log("Result from creating objects using constructor");
console.log(myComputer);
console.log(typeof myComputer);




//Accessing objects


//using dot notation
console.log("Result from dot notation: " + laptop.name);
console.log("Result from dot notation: " + laptop2.manufacturedDate);
//using square brackets = undefined
// console.log("Result from square brackets notation: " + laptop[name]);





//array of objects
/*
	accessing arrays
		Syntax:
		arrayName[index].property
		*/

		let laptops = [laptop,laptop2];

		console.log(laptops[1].name);
		console.log(laptops[0].manufacturedDate);
// console.log(laptops[0] ["manufacturedDate"]);


//SECTION - initializing, adding, deleting and reassigning object properties.

//initialize
let car = {};
console.log(car);



//adding properties
car.name = "Honda Vios";
car.manufacturedDate = 2022;
console.log(car);
/*
	-while using square brackets will give the same feaurtew as using dot notation, it might lead us to create unconventional naming for the properties
	*/

	car["manufactured date"] = 2019;
	console.log(car);
	console.log(car.manufacturedDate);



//deleting of properties
console.log(car);
delete car["manufactured date"];
delete car.manufacturedDate;
console.log(car);

//reassigning of properties

car.name = "Dodge Charger R/T";
console.log(car); 


//Object Methods
//these are useful for creating object-specific functions which are used to perform tasks on them

let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is " + this.name);
	}
}
console.log(person);
console.log("Result from object methods: ")
person.talk();

//adding a method user = instead of : when trying to add another property into the object
person.walk = function() {
	//using template literals backticks + ${} + string data type
	console.log(`${this.name} walked 25 steps`)
	// console.log(this.name + " walked 25 steps")
}
person.walk();


let friend = {
	firstName: "Joe",
	lastName: "Smith",
	// nested object
	address: {
		city: "Austin",
		state: "Texas"
	},
	// nested array
	emails: [ 'joe@mail.com', "johnHandsome@mail.com" ],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
}
friend.introduce();
console.log(friend);

//real-world application of objects

/*
	Scenario
	-we would like to create a game that would have several polemon interact with each other
	-every pokemon would have the same set of stats, properties and functions
	*/


	let myPokemon = {
		name: "Pikachu",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log("This Pokemon tackled targetPokemon");
			console.log("targetPokemon's health is now reduced to_targetPokemonHealth_");
		},
		faint: function(){
			console.log("Pokemon Fainted");
		}
	};
	console.log(myPokemon);



//using constructor function

function Pokemon(name, level){
	//Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;


	//Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth");
	}
	this.faint = function(){
		console.log(this.name + "fainted");
	}
}

//creating new pokemon
let pikachu = new Pokemon("Pikachu", 16);
let ratata = new Pokemon("Ratata", 8);

//tackle method

pikachu.tackle(ratata);